package com.example.individualproject.repository;

import com.example.individualproject.model.Employee;
import com.example.individualproject.repository.EmployeeRepository;
import com.example.individualproject.repository.IEmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

public class EmployeeRepoTest {
    @Mock
    private IEmployeeRepository repository;
    private AutoCloseable autoCloseable;
    private EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp(){
        autoCloseable = MockitoAnnotations.openMocks(this);
        employeeRepository = new EmployeeRepository(repository);
    }

    @AfterEach
    void tearDown() throws Exception{
        autoCloseable.close();
    }

    @Test
    void addEmployee(){
        Employee employee = new Employee("Neyko", "Neykov", "neykneyk", "123pass");

        repository.saveAndFlush(employee);

        ArgumentCaptor<Employee>argumentCaptor = ArgumentCaptor.forClass(Employee.class);
        verify(repository).saveAndFlush(argumentCaptor.capture());
        Employee capturedEmployee = argumentCaptor.getValue();
        assertThat(capturedEmployee).isEqualTo(employee);
    }

    @Test
    void getAllEmployees(){
        employeeRepository.getAllEmployees();
        verify(repository).findAll();
    }

    @Test
    void getEmployeeById(){
        Employee employee = new Employee("Neyko", "Neykov", "neykneyk", "123pass");
        employeeRepository.saveAndFlush(employee);
        repository.saveAndFlush(employee);
        Employee checkEmployee = employeeRepository.getEmployeeById(1);
        verify(repository).getEmployeeById(1);
    }

    @Test
    void getEmployeeByUsername(){
        Employee employee = new Employee("Neyko", "Neykov", "neykneyk", "123pass");
        employeeRepository.saveAndFlush(employee);
        repository.saveAndFlush(employee);
        Employee checkEmployee = employeeRepository.findByUsername("neykneyk");
        verify(repository).findByUsername("neykneyk");
    }
}
