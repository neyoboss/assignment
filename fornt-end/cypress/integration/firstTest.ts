describe('Login page', ()=>{
  it('display login page', ()=>{
    cy.visit('/login')
      .get('h1').contains('Login')
      .get('#username').clear().type("kKolev")
      .get('#password').clear().type("123")
      .get('#loginButton').click();

      cy.hash().should('eq','');
  })
})
