import { Injectable } from '@angular/core';
import {ChatMessgae} from "../models/ChatMessgae";

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {

  webSocket: WebSocket;
  chatMessage: ChatMessgae[] = [];

  constructor() { }

  public openWebSocket(){
    this.webSocket = new WebSocket('ws://localhost:8080/chat');

    this.webSocket.onopen = (event) =>{
      console.log('Open: ', event);
    };

    this.webSocket.onmessage = (event)=>{
      const chatMessage = JSON.parse(event.data);
      this.chatMessage.push(chatMessage);
    }

    this.webSocket.onclose = (event)=>{
      console.log('Close', event);
    }
  }

  public senMessage(message: ChatMessgae){
    this.webSocket.send(JSON.stringify(message));
  }

  public closeWebSocket(){
    this.webSocket.close();
  }
}
