package com.example.individualproject.repository;

import com.example.individualproject.dalInterface.IComputerDal;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ComputerRepository implements IComputerDal {

    @Autowired
    IComputerRepository repo;

    public ComputerRepository(IComputerRepository repository) {
        this.repo = repository;
    }

    @Override
    public List<Computer> getAllComputers() {
        return repo.findAll();
    }

    @Override
    public Computer saveAndFlush(Computer computer) {
        return repo.saveAndFlush(computer);
    }

    @Override
    public Computer getComputerById(long id) {
        return repo.getComputerById(id);
    }


    @Override
    public List<Computer> getComputerByEmployee(Employee employee) {
        return repo.getComputerByEmployee(employee);
    }

}
