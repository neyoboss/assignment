import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  currentAccount: any;

  private api = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
    if (localStorage.getItem('currentAccount')) {
      let accountString: any = localStorage.getItem('currentAccount');
      this.currentAccount = JSON.parse(accountString.toString());
    }
  }

  login(username: string, password: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    this.http.post(`http://localhost:8080/login`,
      {username, password},
      {headers: httpOptions.headers}).subscribe(res => {
      console.log(res)
      localStorage.setItem('currentAccount', JSON.stringify(res));
      console.log(this.currentAccount.Username);
      location.reload();
    });
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentAccount');
     // location.reload()
  }

  //check if the logged in user is admin
  get isAdmin() {
    if (this.currentAccount) {
      if (this.currentAccount.EmployeeRole == "[ADMIN]") {
        return true;
      }
    }
    return false;
  }

  //check if the logged in user is an employee
  get isEmployee() {
    if (this.currentAccount) {
      if (this.currentAccount.EmployeeRole == "[EMPLOYEE]") {
        return true;
      }
    }
    return false;
  }

  //returns the authorization if there is any for the headers of requests
  get auth() {
    if (this.currentAccount) {
      return this.currentAccount.Authorization;
    }
    return null;
  }
}
