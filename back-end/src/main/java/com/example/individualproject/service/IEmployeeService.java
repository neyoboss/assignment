package com.example.individualproject.service;

import com.example.individualproject.DTO.EmployeeDTO;
import com.example.individualproject.model.Employee;

import java.util.List;

public interface IEmployeeService {
    List<EmployeeDTO>getAllEmployees();
    EmployeeDTO saveAndFlush(Employee employee);
    Employee getEmployeeById(long id);
    EmployeeDTO getEmployeeDTOById(long id);
    Employee findByUsername(String username);
    EmployeeDTO getEmployeeByUsername(String username);

}
