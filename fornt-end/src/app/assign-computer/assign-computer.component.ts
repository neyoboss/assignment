import {Component, Inject, OnInit} from '@angular/core';
import {ComputerService} from "../service/computer.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {HttpErrorResponse} from "@angular/common/http";
import {Computer} from "../models/Computer";

@Component({
  selector: 'app-assign-computer',
  templateUrl: './assign-computer.component.html',
  styleUrls: ['./assign-computer.component.css']
})
export class AssignComputerComponent implements OnInit {

  computer: Computer;
  computerArray: any;
  justAList:any[] = [];

  constructor(private computerService: ComputerService,
              private dialogRef: MatDialogRef<AssignComputerComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit(): void {
    this.computerService.getComputers().subscribe((data) => {
        this.computerArray = data;
        console.log(this.computerArray)
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      });
  }

  onAssign(id:number) {
    this.computer.assigned = true;
    this.justAList.push(this.computer);
    this.computerService.assignComputer(id,this.justAList).subscribe(()=>{
      console.log("Computer assigned");
    });
    location.reload();
  }

  onCancel() {
    this.dialogRef.close();
  }
}
