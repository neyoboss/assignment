import { Component, OnInit } from '@angular/core';
import {ComputerService} from "../service/computer.service";
import {MatDialogRef} from "@angular/material/dialog";
import {Computer} from "../models/Computer";
import {FormControl, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-add-computer',
  templateUrl: './add-computer.component.html',
  styleUrls: ['./add-computer.component.css']
})
export class AddComputerComponent implements OnInit {

  computer:Computer;

  model = new FormControl('',[Validators.required]);
  problem = new FormControl('',[Validators.required]);
  initialPrice = new FormControl('',[Validators.required]);

  constructor(private service:ComputerService,
              private dialog:MatDialogRef<AddComputerComponent>) { }

  ngOnInit(): void {
  }

  onAdd(){
    this.computer = new Computer();
    this.computer.model = this.model.value.toString();
    this.computer.problem = this.problem.value.toString();
    this.computer.initialPrice = this.initialPrice.value;
    this.service.addComputer(this.computer).subscribe((data)=>{
      console.log(data);
    },
      (error:HttpErrorResponse)=>{
      console.log(error);
      });
    location.reload();
  }

  onCancel(){
    this.dialog.close();
  }
}
