import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignComputerComponent } from './assign-computer.component';

describe('AssignComputerComponent', () => {
  let component: AssignComputerComponent;
  let fixture: ComponentFixture<AssignComputerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AssignComputerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignComputerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
