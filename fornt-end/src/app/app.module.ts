import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ItemTableComponent} from './item-table/item-table.component';
import {HttpClientModule} from "@angular/common/http";
import {PartService} from "./service/part.service";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SideNavComponent} from "./side-nav/side-nav.component";
import {EmpployeeTableComponent} from './empployee-table/empployee-table.component';
import {EmployeeService} from "./service/employee.service";
import {RouterModule} from '@angular/router';
import {MatTableModule} from "@angular/material/table";
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from "@angular/material/list";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSortModule} from "@angular/material/sort";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DeleteButtonComponent} from './delete-button/delete-button.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {EditButtonComponent} from './edit-button/edit-button.component';
import {MatSelectModule} from "@angular/material/select";
import {AddButtonComponent} from './add-button/add-button.component';
import {AddTagButtonComponent} from './edit-button/add-tag-button/add-tag-button.component';
import {MatCardModule} from "@angular/material/card";
import {MatRadioModule} from "@angular/material/radio";
import {AddEmployeeComponent} from './add-employee/add-employee.component';
import {AssignComputerComponent} from './assign-computer/assign-computer.component';
import {ComputersTableComponent} from './computers-table/computers-table.component';
import { AddComputerComponent } from './add-computer/add-computer.component';
import { LoginComponent } from './login/login.component';
import { ProfilePageComponent } from './profile-page/profile-page.component';
import { AssignPartComponent } from './assign-part/assign-part.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import { ChatComponent } from './chat/chat.component';


@NgModule({
  declarations: [
    AppComponent,
    ItemTableComponent,
    SideNavComponent,
    EmpployeeTableComponent,
    DeleteButtonComponent,
    EditButtonComponent,
    AddButtonComponent,
    AddTagButtonComponent,
    AddEmployeeComponent,
    AssignComputerComponent,
    ComputersTableComponent,
    AddComputerComponent,
    LoginComponent,
    ProfilePageComponent,
    AssignPartComponent,
    ChatComponent,

  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        BrowserAnimationsModule,
        RouterModule.forRoot([
            {path: 'employee', component: EmpployeeTableComponent},
            {path: 'part', component: ItemTableComponent},
            {path: 'computers', component: ComputersTableComponent},
            {path: 'login', component: LoginComponent},
            {path: 'profile-page', component: ProfilePageComponent},
            {path: 'chat', component: ChatComponent}
        ]),
        MatTableModule,
        MatSidenavModule,
        MatListModule,
        MatToolbarModule,
        MatSortModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatDialogModule,
        MatButtonModule,
        MatIconModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatCardModule,
        MatRadioModule,
        MatCheckboxModule,
    ],
  providers: [PartService, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
