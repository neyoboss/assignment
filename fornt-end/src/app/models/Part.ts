import {Tag} from "./Tag";

export class Part{
  constructor() {
    this.tagsList = [];
  }

  id:number;
  tagsList: Tag[] = [];
  brand: string;
  model: string;
  price : number;
  quantity : number;
  description:string;
}
