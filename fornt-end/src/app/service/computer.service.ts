import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Computer} from "../models/Computer";

@Injectable({
  providedIn: 'root'
})
export class ComputerService {

  private api = environment.apiBaseUrl;

  constructor(private http: HttpClient) {
  }

  public getComputers(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.api}/computer`, {headers: httpOptions.headers});
  }

  public getComputerById(id:number): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.api}/computer/${id}`, {headers: httpOptions.headers});
  }

  public getComputerPartsById(id:number):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.get<any>(`${this.api}/computer/getAssignedParts/${id}`,{headers: httpOptions.headers})
  }

  public addComputer(computer: Computer): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(`${this.api}/computer/addComputer`, computer, {headers: httpOptions.headers});
  }

  public assignComputer(id: number, computer: Computer[]): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post(`${this.api}/computer/assignComputer/${id}`, computer, {headers: httpOptions.headers});
  }

  public upadteComputer(computerId: number, partId: number, fixed:boolean): Observable<any> {
    const params = new HttpParams({
      fromObject: {
        computerId: computerId,
        partId: partId,
        fixed:fixed
      }
    })
    return this.http.post<any>(`${this.api}/computer/assignPart`, null, {params: params});
  }

  public removePartFromPC(computerId: number, partId: number): Observable<any> {
    const params = new HttpParams({
      fromObject: {
        computerId: computerId,
        partId: partId,
      }
    })
    return this.http.post<any>(`${this.api}/computer/removePartFromPc`, null, {params: params});
  }
}
