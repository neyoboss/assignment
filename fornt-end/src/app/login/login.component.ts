import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from "@angular/forms";
import {LoginService} from "../service/login.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  returnUrl: string;

  hidePass = true;
  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);

  currentAccount: any;

  constructor(private loginService: LoginService,
              private router: Router,
              private route: ActivatedRoute) {
    if (localStorage.getItem('currentAccount')) {
      this.router.navigateByUrl('/profile-page');
    }
  }

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  onLogin() {
    let username: string = this.username.value;
    let password: string = this.password.value;

    this.loginService.login(username,password);

  }
}
