import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PartService} from "../service/part.service";
import {FormControl, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {MatTableDataSource} from "@angular/material/table";
import {Tag} from "../models/Tag";
import {HttpErrorResponse} from "@angular/common/http";
import {Part} from "../models/Part";

@Component({
  selector: 'app-edit-button',
  templateUrl: './edit-button.component.html',
  styleUrls: ['./edit-button.component.css']
})
export class EditButtonComponent implements OnInit {
  displayColumns: string[] = ['name', 'action'];

  public dataSource!: MatTableDataSource<Tag>;
  private dataArray: any;


  tag: Tag;
  part:Part;

  name = new FormControl('', [Validators.required]);
  formControl = new FormControl('', [Validators.required])

  constructor(private dialog: MatDialogRef<EditButtonComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private partService: PartService) {
  }


  ngOnInit(): void {

    this.partService.getTags(this.data.id).subscribe((data) => {
        console.log(data);
        this.dataArray = data;
        this.dataSource = new MatTableDataSource<Tag>(this.dataArray)
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    );
  }

  onAddTag(id: number): void {

    this.tag = new Tag();
    this.tag.name = this.name.value.toString();

    this.dataArray.push(this.tag);

    this.partService.addTags(id, this.dataArray).subscribe(() => {
        console.log("Tag added")
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      });
  }

  onDeleteTag(id: number): void {
    this.partService.deleteTag(id).subscribe(() => {
        console.log("Tag deleted");
      },
      (error: HttpErrorResponse) => {
        console.log(error);
      });
  }

  onCancel(): void {
    this.dialog.close();
  }

  onUpdate(id: number): void {
    this.part = this.data;
    console.log(this.part);
    this.partService.updatePart(id,this.part).subscribe(() =>{
      console.log("Part updated");
    },
      (error: HttpErrorResponse) =>{
      console.log(error);
      });
    location.reload();
  }


  errorMessage() {
    return this.formControl.hasError('requried') ? 'Required field' : '';
  }
}
