describe('Posting part', ()=>{
  context('When I send POST /addPart', ()=>{
    it('Should create part', ()=>{
      cy.request({
        method: 'POST',
        url: 'http://localhost:8080/part/addPart',
        body:{
          brand: "CypressBrand",
          model: "CypressModel",
          price: 45,
          quantity: 5,
          description: "Cypress description"
        }
      })
        .should((response) =>{
          expect((response.status)).eq(200);
        })
    })
  })
})
