package com.example.individualproject.repository;

import com.example.individualproject.model.Computer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

class ComputerRepositoryTest {

    @Mock
    private IComputerRepository repository;
    private IEmployeeRepository iEmployeeRepository;
    private AutoCloseable autoCloseable;
    private ComputerRepository computerRepository;

    @BeforeEach
    void setUp(){
        autoCloseable = MockitoAnnotations.openMocks(this);
        computerRepository = new ComputerRepository(repository);
    }

    @AfterEach
    void tearDown() throws Exception{
        autoCloseable.close();
    }

    @Test
    void getAllComputers() {
        computerRepository.getAllComputers();
        verify(repository).findAll();
    }

    @Test
    void addComputer() {
        Computer computer = new Computer("Lenovo","broken","some description",400,false,false,0);
        repository.saveAndFlush(computer);
        ArgumentCaptor<Computer> argumentCaptor = ArgumentCaptor.forClass(Computer.class);
        verify(repository).saveAndFlush(argumentCaptor.capture());
        Computer capturedComputer = argumentCaptor.getValue();
        assertThat(capturedComputer).isEqualTo(computer);
    }

    @Test
    void getComputerById() {
        Computer computer = new Computer("Lenovo","broken","some description",400,false,false,0);
        computerRepository.saveAndFlush(computer);
        repository.saveAndFlush(computer);
        Computer checkComputer = computerRepository.getComputerById(1);
        verify(repository).getComputerById(1);
    }

//    @Test
//    void getComputerByEmployee() {
//        Computer computer = new Computer("Lenovo","broken","some description",400,false,false,0);
//        Employee employee = new Employee("Neyko", "Neykov", "neykneyk", "123pass");
//        List<Computer> computers = new ArrayList<>();
//        computers.add(computer);
//        computer.setEmployee(employee);
//        employee.setComputerList(computers);
//        computerRepository.saveAndFlush(computer);
//        iEmployeeRepository.saveAndFlush(employee);
//        verify(repository).getComputerByEmployee(employee);
//    }
}