import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmpployeeTableComponent } from './empployee-table.component';

describe('EmpployeeTableComponent', () => {
  let component: EmpployeeTableComponent;
  let fixture: ComponentFixture<EmpployeeTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmpployeeTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmpployeeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
