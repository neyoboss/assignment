package com.example.individualproject.repository;

import com.example.individualproject.dalInterface.ITagDal;
import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class TagRepository implements ITagDal {

    @Autowired
    ITagRepository repository;

    public TagRepository(ITagRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Tag> getAllTags() {
        return repository.findAll();
    }

    @Override
    public void addTag(Tag tag) {
        repository.save(tag);
    }

    @Override
    public Tag getTagById(long id) {
        return repository.getById(id);
    }

    @Override
    public List<Tag> saveAndFlush(List<Tag> tagList) {
        return repository.saveAllAndFlush(tagList);
    }

    @Override
    public List<Tag> getByPart(Part part) {
        return repository.getByPart(part);
    }

    @Override
    public void deleteTag(long id) {
        repository.delete(repository.getById(id));
    }
}
