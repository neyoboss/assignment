package com.example.individualproject.service;

import com.example.individualproject.DTO.PartDTO;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Part;

import java.util.List;

public interface IPartService {
    List<PartDTO>getAllParts();
    boolean deletePart(long id);
    PartDTO saveAndFlush(Part part);
    PartDTO getPartDTOById(long id);
    Part getPartById(long id);
    List<PartDTO>getByComputer(Computer computer);
}
