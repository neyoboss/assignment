import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../service/employee.service";
import {HttpErrorResponse} from "@angular/common/http";
import {Employee} from "../models/Employee";
import {MatTableDataSource} from "@angular/material/table";
import {Computer} from "../models/Computer";
import {MatDialog} from "@angular/material/dialog";
import {AddEmployeeComponent} from "../add-employee/add-employee.component";
import {AssignComputerComponent} from "../assign-computer/assign-computer.component";
import {ComputerService} from "../service/computer.service";

@Component({
  selector: 'app-empployee-table',
  templateUrl: './empployee-table.component.html',
  styleUrls: ['./empployee-table.component.css']
})
export class EmpployeeTableComponent implements OnInit {

  displayedColumns: string[] = ['model', 'problem', 'description', 'initialPrice', 'finalPrice', 'isFixed'];

  dataArray: any; //employee list
  employeeDetails: Employee = new Employee();

  computerList: MatTableDataSource<Computer>;//computerTable

  isAdmin: any;
  showForm: boolean = false;
  readonly: boolean = true;

  constructor(private employeeService: EmployeeService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.employeeService.getEmployees().subscribe(
      (data) => {
        console.log(data);
        this.dataArray = data;
      },
      (err: HttpErrorResponse) => {
        console.log(err);
      }
    );
  }


  displayEmployeeInfo(id: number) {
    this.employeeService.getEmployeeById(id).subscribe((data) => {
        console.log((data));
        this.employeeDetails = data;
        this.computerList = new MatTableDataSource<Computer>(data.computerList)
      });

    this.isAdmin = this.employeeDetails.employeeRole;
  }

  onAddEmployee() {
    this.dialog.open(AddEmployeeComponent);
  }

  onAssignComputer(id:number) {
    this.dialog.open(AssignComputerComponent,{data:{id:id}});
  }
}

