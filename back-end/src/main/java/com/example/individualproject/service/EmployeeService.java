package com.example.individualproject.service;

import com.example.individualproject.DTO.EmployeeDTO;
import com.example.individualproject.dalInterface.IEmployeeDal;
import com.example.individualproject.model.Employee;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService implements IEmployeeService {
    @Autowired
    IEmployeeDal dal;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Override
    public List<EmployeeDTO> getAllEmployees() {
        return dal.getAllEmployees().stream().map(this::EntityDTO).collect(Collectors.toList());
    }

    @Override
    public EmployeeDTO saveAndFlush(Employee employee) {
        employee.setPassword(passwordEncoder.encode(employee.getPassword()));
        return EntityDTO(dal.saveAndFlush(employee));
    }

    @Override
    public Employee getEmployeeById(long id) {
        return dal.getEmployeeById(id);
    }


    @Override
    public EmployeeDTO getEmployeeDTOById(long id) {
        return EntityDTO(dal.getEmployeeById(id));
    }

    @Override
    public Employee findByUsername(String username) {
        return dal.findByUsername(username);
    }

    @Override
    public EmployeeDTO getEmployeeByUsername(String username) {
        return EntityDTO(dal.getEmployeeByUsername(username));
    }

    public EmployeeDTO EntityDTO(Employee employee){
        EmployeeDTO employeeDTO = modelMapper.map(employee, EmployeeDTO.class);
        return  employeeDTO;
    }
}
