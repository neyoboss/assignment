package com.example.individualproject.DTO;

import com.example.individualproject.model.Part;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ComputerDTO {
    private long id;
    private String model;
    private String problem;
    private String description;
    private double initialPrice;
    private boolean fixed;
    private boolean assigned;
    private double finalPrice;
    private List<Part>usedPartsToFix;
}

