import {Part} from "./Part";
import {Employee} from "./Employee";

export class Computer{
  constructor() {
    this.usedPartsToFix = [];
  }
  employee:Employee;
  id:number;
  model:string;
  problem:string;
  description:string;
  initialPrice:number;
  fixed:boolean;
  assigned:boolean;
  finalPrice:number;
  usedPartsToFix: Part[] = [];
}
