import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ItemTableComponent} from "./item-table/item-table.component";
import {EmpployeeTableComponent} from "./empployee-table/empployee-table.component";

const routes: Routes = [
  {path: 'part', component: ItemTableComponent},
  {path: 'employee', component: EmpployeeTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
