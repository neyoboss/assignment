package com.example.individualproject.dalInterface;

import com.example.individualproject.model.Employee;

import java.util.List;

public interface IEmployeeDal {
    List<Employee>getAllEmployees();
    Employee saveAndFlush(Employee employee);
    Employee getEmployeeById(long id);
    Employee findByUsername(String username);
    Employee getEmployeeByUsername(String username);
}
