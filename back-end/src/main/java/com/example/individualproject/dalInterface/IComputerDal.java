package com.example.individualproject.dalInterface;

import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;

import java.util.List;

public interface IComputerDal {
    List<Computer> getAllComputers();
    Computer saveAndFlush(Computer computer);
    Computer getComputerById(long id);
    List<Computer>getComputerByEmployee(Employee employee);
}
