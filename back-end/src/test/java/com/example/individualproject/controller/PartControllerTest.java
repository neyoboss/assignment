//package com.example.individualproject.controller;
//
//import com.example.individualproject.DTO.PartDTO;
//import com.example.individualproject.config.AuthenticationEmployeeService;
//import com.example.individualproject.model.Part;
//import com.example.individualproject.service.PartService;
//import org.junit.jupiter.api.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import java.util.List;
//
//import static org.hamcrest.Matchers.hasSize;
//import static org.hamcrest.Matchers.is;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@WebMvcTest(PartController.class)
//class PartControllerTest {
//    @Autowired
//    private MockMvc mvc;
//
//    @MockBean
//    private PartService service;
//
//    @Test
//    void getParts() throws Exception {
//        Part part = new Part("AMD","Ryzen 5", 400, "it is CPU", 15);
//        List<PartDTO> parts = List.of(service.EntityToDTO(part));
//        given(service.getAllParts()).willReturn(parts);
//
//        mvc.perform(get("/part")
//                .contentType(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$", hasSize(1)))
//                .andExpect(jsonPath("$[0].brand", is(part.getBrand())));
//    }
//
//    @Test
//    void add() {
//    }
//}