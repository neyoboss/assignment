import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Part} from "../models/Part";
import {environment} from "../../environments/environment";
import {Tag} from "../models/Tag";

@Injectable({
  providedIn: 'root'
})
export class PartService {
  private apiServerUrl = environment.apiBaseUrl;

  constructor(private http: HttpClient) {}



  public getParts(): Observable<Part>{
    const URL = `${this.apiServerUrl}/part`;
    return this.http.get<any>(URL);

  }

  public addTags(id:number,tag:Tag[]):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<any[]>(`${this.apiServerUrl}/part/addPartTag/${id}`, tag,{headers:httpOptions.headers});
  }

  public getTags(id:number):Observable<any>{
    return this.http.get<any>(`${this.apiServerUrl}/part/tag/${id}`);
  }

  public addPart(part: Part):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`${this.apiServerUrl}/part/addPart`,part,{headers:httpOptions.headers});
  }

  deleteTag(id:number):Observable<any>{
    return this.http.delete(`${this.apiServerUrl}/part/deleteTag/${id}`,{responseType:'text'});
  }

  deletePart(id:number):Observable<any>{
    return this.http.delete(`${this.apiServerUrl}/part/deletePart/${id}`, {responseType: 'text'});
  }

  updatePart(id:number, part:Part):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`${this.apiServerUrl}/part/updatePart/${id}`,part,{headers:httpOptions.headers});
  }
}
