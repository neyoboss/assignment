package com.example.individualproject.repository;

import com.example.individualproject.dalInterface.IPartDal;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Part;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PartRepository implements IPartDal {


    @Autowired
    IPartRepository repo;

    public PartRepository(IPartRepository repository) {
        this.repo = repository;
    }

    @Override
    public List<Part> getAllParts() {
        return repo.findAll();
    }

    @Override
    public Part saveAndFlush(Part part) {
        return repo.saveAndFlush(part);
    }

    @Override
    public Part getPartById(long id) {
        return repo.getPartById(id);
    }

    @Override
    public List<Part> getByComputer(Computer computer) {
        return repo.getByComputer(computer);
    }

    @Override
    public  boolean deletePart(long id){
        repo.delete(repo.getById(id));
        return true;
    }

}
