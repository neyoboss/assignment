import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../service/employee.service";
import {MatDialogRef} from "@angular/material/dialog";
import {Employee} from "../models/Employee";
import {FormControl, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {

  employee:Employee;

  firstName = new FormControl('',[Validators.required]);
  lastName = new FormControl('',[Validators.required]);
  username = new FormControl('',[Validators.required]);
  password = new FormControl('',[Validators.required]);
  employeeRole = new FormControl('',[Validators.required]);

  constructor(private employeeService: EmployeeService,
              private dialog:MatDialogRef<AddEmployeeComponent>) { }

  ngOnInit(): void {
  }

  onAdd(){
    this.employee = new Employee();
    this.employee.firstName = this.firstName.value.toString();
    this.employee.lastName = this.lastName.value.toString();
    this.employee.username = this.username.value.toString();
    this.employee.password = this.password.value.toString();
    if (this.employeeRole.value === true){
      this.employee.employeeRole = "ADMIN";
    }
    else {
      this.employee.employeeRole = "EMPLOYEE";
    }

    this.employeeService.addEmployee(this.employee).subscribe(()=> {
      console.log("Employee added")
    },
      (error:HttpErrorResponse)=>{
      console.log(error);
      })
    location.reload();
  }

  onCancel(){
    this.dialog.close();
  }
}
