package com.example.individualproject.dalInterface;

import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;

import java.util.List;

public interface ITagDal {
    List<Tag> getAllTags();
    void addTag(Tag tag);
    Tag getTagById(long id);
    List<Tag> saveAndFlush(List<Tag> tagList);
    List<Tag>getByPart(Part part);
    void deleteTag(long id);
}
