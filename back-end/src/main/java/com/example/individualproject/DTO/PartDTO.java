package com.example.individualproject.DTO;

import com.example.individualproject.model.Tag;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PartDTO {
    private long id;
    private List<Tag> tags;
    private String brand;
    private String model;
    private double price;
    private double quantity;
    private String description;
}
