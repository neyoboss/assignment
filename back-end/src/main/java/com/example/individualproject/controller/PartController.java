package com.example.individualproject.controller;

import com.example.individualproject.DTO.PartDTO;
import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;
import com.example.individualproject.service.IPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("part")
public class PartController {

    @Autowired
    private IPartService service;

    @GetMapping()
    public @ResponseBody List<PartDTO> getAllParts(){
        return service.getAllParts();
    }

    @RequestMapping(value = "/{partId}", method = RequestMethod.GET)
    public @ResponseBody PartDTO partById(@PathVariable() long partId){
        return service.getPartDTOById(partId);
    }

    @RequestMapping(value = "/addPart", method = RequestMethod.POST)
    public @ResponseBody String add(@RequestBody Part part){
        service.saveAndFlush(part);
        return "Part added";
    }

    @RequestMapping(value = "/updatePart/{partId}", method = RequestMethod.POST)
    public @ResponseBody String updatePart(@PathVariable long partId, @RequestBody Part part){
        service.saveAndFlush(part);
        return "Part updated";
    }

    @RequestMapping(value = "/addPartTag/{partId}", method = RequestMethod.POST)
    public @ResponseBody String addPartTag(@PathVariable long partId,@RequestBody List<Tag> tags){

        Part part = service.getPartById(partId);
        for (Tag tagtemp: tags) {
            tagtemp.setPart(part);
        }

        part.setTags(tags);
        service.saveAndFlush(part);
        return "Tags added";
    }

    @RequestMapping(value = "/deletePart/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String delete(@PathVariable long id){
        service.deletePart(id);
        return "deleted";
    }
}