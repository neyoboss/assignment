import {Component, Inject, OnInit} from '@angular/core';
import {PartService} from "../service/part.service";
import {Part} from "../models/Part";
import {FormControl, Validators} from "@angular/forms";
import {ComputerService} from "../service/computer.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {Computer} from "../models/Computer";
import {MatTableDataSource} from "@angular/material/table";
import {HttpErrorResponse} from "@angular/common/http";
import {bottom} from "@popperjs/core";

@Component({
  selector: 'app-assign-part',
  templateUrl: './assign-part.component.html',
  styleUrls: ['./assign-part.component.css']
})
export class AssignPartComponent implements OnInit {

  displayedColumns: string[] = ['model', 'price', 'action'];
  assignPartList: MatTableDataSource<Part>;

  computer: any;
  partList: any;
  part: any;

  fixed = new FormControl('',[Validators.required]);

  constructor(private partService: PartService,
              private computerService: ComputerService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private dialog: MatDialogRef<AssignPartComponent>) {
  }

  ngOnInit(): void {
    this.partService.getParts().subscribe(data => {
      this.partList = data;
    });

    this.computerService.getComputerPartsById(this.data.id).subscribe(part => {
      this.part = part;
      this.assignPartList = new MatTableDataSource<Part>(part);
    });
  }

  onAssign(computerId: number, partId: number) {
    this.computerService.upadteComputer(computerId, partId, this.fixed.value).subscribe(() => {
      console.log("Part assigned")
    });
    console.log(this.data.id, this.part.id);
    location.reload();
  }

  onRemovePart(computerId: number, partId: number){
    this.computerService.removePartFromPC(computerId, partId).subscribe();
    location.reload();
  }

  onCancel() {
    this.dialog.close();
  }
}
