
describe('Load parts', () => {
  context('GET /part', () => {
    it('fetches Parts - GET', () => {
      cy.visit('/part');
      cy.request({
        method:'GET',
        url:'http://localhost:8080/part'
      })
        .should((response) =>{
          // cy.log(JSON.stringify(response.body))
          expect(response.status).to.eq(200);
        });
    });
  });
});
