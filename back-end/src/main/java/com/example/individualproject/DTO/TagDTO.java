package com.example.individualproject.DTO;

import lombok.Data;

@Data
public class TagDTO {
    private long id;
    private String name;
}
