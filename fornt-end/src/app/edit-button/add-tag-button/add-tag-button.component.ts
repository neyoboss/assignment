import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {Tag} from "../../models/Tag";

@Component({
  selector: 'app-add-tag-button',
  templateUrl: './add-tag-button.component.html',
  styleUrls: ['./add-tag-button.component.css']
})
export class AddTagButtonComponent implements OnInit {

  constructor(private dialog:MatDialogRef<AddTagButtonComponent>) { }

  ngOnInit(): void {
  }

  onAdd():void{

  }

  onCancel():void{
    this.dialog.close();
  }

}
