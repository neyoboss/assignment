describe('Load employees', () => {
  context('When i send GET /employees', () => {
    it('fetches Employees', () => {
      cy.visit('/employee');
      cy.request({
        method: 'GET',
        url: 'http://localhost:8080/employee'
      })
        .should((response) => {
          // cy.log(JSON.stringify(response.body))
          expect(response.status).to.eq(200);
        });
    });
  });
});
