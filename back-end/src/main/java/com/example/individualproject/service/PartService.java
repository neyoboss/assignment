package com.example.individualproject.service;

import com.example.individualproject.DTO.PartDTO;
import com.example.individualproject.dalInterface.IPartDal;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Part;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PartService implements IPartService{

    @Autowired
    IPartDal dal;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<PartDTO> getAllParts() {
       return dal.getAllParts()
               .stream()
               .map(this::EntityToDTO)
               .collect(Collectors.toList());
    }

    @Override
    public PartDTO saveAndFlush(Part part) {
       return EntityToDTO(dal.saveAndFlush(part));
    }

    @Override
    public PartDTO getPartDTOById(long id) {
        return EntityToDTO(dal.getPartById(id));
    }

    @Override
    public Part getPartById(long id) {
        return dal.getPartById(id);
    }

    @Override
    public List<PartDTO> getByComputer(Computer computer) {
        return dal.getByComputer(computer)
                .stream()
                .map(this::EntityToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public boolean deletePart(long id){
        dal.deletePart(id);
        return true;
    }

    public PartDTO EntityToDTO(Part part) {
        PartDTO partDTO = modelMapper.map(part, PartDTO.class);
        return partDTO;
    }
}
