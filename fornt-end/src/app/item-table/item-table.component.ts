import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Part} from "../models/Part";
import {PartService} from "../service/part.service";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {MatTableDataSource, MatTableModule} from '@angular/material/table';
import {Subscription} from "rxjs";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {DeleteButtonComponent} from "../delete-button/delete-button.component";
import {EditButtonComponent} from "../edit-button/edit-button.component";
import {Tag} from "../models/Tag";
import {AddButtonComponent} from "../add-button/add-button.component";

@Component({
  selector: 'app-item-table',
  templateUrl: './item-table.component.html',
  styleUrls: ['./item-table.component.css']
})
export class ItemTableComponent implements OnInit{

  displayedColumns: string[] = ['brand', 'model', 'price','quantity','actions'];
  public dataSource!: MatTableDataSource<Part>;
  private dataArray: any;
  id: number;

  constructor(private partService: PartService,
              private dialogService: MatDialog,
              public http: HttpClient) {}

  @ViewChild(MatSort) sort!: MatSort;

  ngOnInit() {
    this.partService.getParts().subscribe((data) =>{
      console.log(data);
      this.dataArray = data;
      this.dataSource = new MatTableDataSource<Part>(this.dataArray);

      this.dataSource.sort = this.sort;

    },
      (err: HttpErrorResponse) =>{
      console.log(err);
      }
    );
  }

  deletePart(id:number):void{
    this.id = id;
    this.dialogService.open(DeleteButtonComponent,{data: {id:id}});
  }

  addPart():void{
    this.dialogService.open(AddButtonComponent);
  }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
    console.log(filterValue)
  }


  edit(i:number, id:number, productID:number, brand:string, model:string, price:number, quantity:number,description:string){
    this.dialogService.open(EditButtonComponent,{
      data:{id:id, productID:productID, brand:brand, model:model, price:price, quantity:quantity,description:description}
    });
  }
}
