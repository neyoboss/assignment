package com.example.individualproject.DTO;

import com.example.individualproject.model.Computer;
import com.example.individualproject.model.EmployeeRole;
import lombok.Data;

import java.util.List;

@Data
public class EmployeeDTO {
    private long id;
    private String firstName;
    private String lastName;
    private String username;
    private EmployeeRole employeeRole;
    private List<Computer>computerList;
}
