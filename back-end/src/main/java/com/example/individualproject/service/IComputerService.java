package com.example.individualproject.service;

import com.example.individualproject.DTO.ComputerDTO;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;

import java.util.List;

public interface IComputerService {
    List<ComputerDTO>getAllComputers();
    ComputerDTO saveAndFlush(Computer computer);
    Computer getComputerById(long id);
    ComputerDTO getComputerDTOById(long id);
    List<ComputerDTO>getByEmployee(Employee employee);

}
