package com.example.individualproject.repository;

import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IComputerRepository extends JpaRepository<Computer, Long> {
    Computer getComputerById(long id);
    List<Computer>getComputerByEmployee(Employee employee);
}
