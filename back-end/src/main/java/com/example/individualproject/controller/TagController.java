package com.example.individualproject.controller;

import com.example.individualproject.DTO.TagDTO;
import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;
import com.example.individualproject.service.IPartService;
import com.example.individualproject.service.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("part")
public class TagController {
    @Autowired
    IPartService service;

    @Autowired
    ITagService tagService;

    @GetMapping(value = "/tag/{partId}")
    public ResponseEntity<List<TagDTO>> getAllTags(@PathVariable long partId) {
        Part part = service.getPartById(partId);
        List<TagDTO>tagList = tagService.getByPart(part);

        if (tagList != null) {
            return ResponseEntity.ok().body(tagList);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/addTag/{partId}", method = RequestMethod.POST)
    public @ResponseBody String addTag(@PathVariable() long partId, @RequestBody Tag tag){
        Part part = service.getPartById(partId);
        List<Tag> tags = part.getTags();
        tags.add(tag);
        service.saveAndFlush(part);
        return "Tag added";
    }

    @RequestMapping(value = "/deleteTag/{id}", method = RequestMethod.DELETE)
    public @ResponseBody String deleteTag(@PathVariable() long id){
        tagService.deleteTag(id);
        return "Tag deleted";
    }

    @RequestMapping(value = "/editTag/{id}",method = RequestMethod.PUT)
    public @ResponseBody String editTag(@PathVariable() long id){
        Tag tag = tagService.findTagById(id);
        
        return "tag updated";
    }
}
