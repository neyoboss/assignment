package com.example.individualproject.service;

import com.example.individualproject.DTO.TagDTO;
import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;

import java.util.List;

public interface ITagService {
    List<Tag> getAllTags();
    void addTag(Tag tag);
    Tag findTagById(long id);
    List<TagDTO> saveAndFlush(List<Tag> tagList);
    List<TagDTO> getByPart(Part part);
    void deleteTag(long id);
}
