package com.example.individualproject.repository;

import com.example.individualproject.model.Employee;
import com.example.individualproject.model.Tag;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.LIST;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

class TagRepositoryTest {

    @Mock
    private ITagRepository repository;
    private AutoCloseable autoCloseable;
    private TagRepository tagRepository;

    @BeforeEach
    void setUp(){
        autoCloseable = MockitoAnnotations.openMocks(this);
        tagRepository = new TagRepository(repository);
    }

    @AfterEach
    void tearDown() throws Exception{
        autoCloseable.close();
    }

    @Test
    void getAllTags() {
        tagRepository.getAllTags();
        verify(repository).findAll();
    }

    @Test
    void addTag() {
        Tag tag = new Tag("tag");
        repository.saveAndFlush(tag);
        ArgumentCaptor<Tag> argumentCaptor = ArgumentCaptor.forClass(Tag.class);
        verify(repository).saveAndFlush(argumentCaptor.capture());
        Tag capturedTag = argumentCaptor.getValue();
        assertThat(capturedTag).isEqualTo(tag);
    }

}