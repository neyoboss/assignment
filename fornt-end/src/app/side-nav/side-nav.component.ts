import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {BreakpointObserver, Breakpoints} from "@angular/cdk/layout";
import {map} from "rxjs/operators";
import {Router} from "@angular/router";
import {LoginService} from "../service/login.service";

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  isHandset: Observable<boolean> = this.breakpoint.observe(Breakpoints.Handset).pipe(map(result => result.matches));

  constructor(private breakpoint: BreakpointObserver,
              private router: Router,
              private loginService: LoginService) {
  }

  ngOnInit(): void {
  }

  isEmployeeLogged() {
    if (localStorage.getItem('currentAccount') != null) {
      return true;
    } else {
      return false;
    }
  }

  get isAdmin() {
    if (this.loginService.isAdmin) {
      return true;
    } else {
      return false;
    }
  }

  get isEmployee() {
    if (this.loginService.isEmployee) {
      return true;
    } else {
      return false;
    }
  }

  onLogout() {
    this.loginService.logout();
    this.router.navigateByUrl('/login')

  }
}
