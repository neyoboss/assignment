package com.example.individualproject.config;

public class SecurityConstraint {

    public static final String SECRET = "employee_secret";
    public static final long EXPIRATION_TIME = 864000000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "employee/addEmployee";
}
