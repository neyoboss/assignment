package com.example.individualproject.dalInterface;

import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Part;

import java.util.List;

public interface IPartDal {
    List<Part> getAllParts();
    boolean deletePart(long id);
    Part saveAndFlush(Part part);
    Part getPartById(long id);
    List<Part>getByComputer(Computer computer);
}
