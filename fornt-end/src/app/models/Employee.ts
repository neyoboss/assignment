import {Computer} from "./Computer";

export class Employee{
  constructor() {
    this.computerList = [];
  }
  id:number;
  firstName: string;
  lastName:string;
  username:string;
  password:string;
  employeeRole:string;
  computerList:Computer[]=[];
}

enum EmployeeRole{
  ADMIN,
  EMPLOYEE
}
