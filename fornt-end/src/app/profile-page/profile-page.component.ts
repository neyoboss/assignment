import {Component, OnInit} from '@angular/core';
import {EmployeeService} from "../service/employee.service";
import {MatTableDataSource} from "@angular/material/table";
import {Computer} from "../models/Computer";
import {MatDialog} from "@angular/material/dialog";
import {AssignPartComponent} from "../assign-part/assign-part.component";
import {FormControl, Validators} from "@angular/forms";
import {Employee} from "../models/Employee";
import {bottom} from "@popperjs/core";

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css']
})
export class ProfilePageComponent implements OnInit {

  displayedColumns: string[] = ['model', 'problem', 'initialPrice', 'finalPrice', 'isFixed', 'action'];

  employee: Employee = new Employee();
  computerList: MatTableDataSource<Computer>;
  readonly: boolean = true;
  visible:boolean = false;

  constructor(private employeeService: EmployeeService,
              private dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.loadEmployee();
  }

  loadEmployee() {
    this.employeeService.getEmployeeByUsername().subscribe(data => {
      this.employee = data;
      console.log(this.employee);
      this.computerList = new MatTableDataSource<Computer>(data.computerList);
    })
  }

  onAssignPart(id: number, finalPrice: number) {
    this.dialog.open(AssignPartComponent, {
      data: {id: id, finalPrice: finalPrice}
    });
  }
}
