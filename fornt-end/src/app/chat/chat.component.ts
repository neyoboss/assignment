import {Component, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {WebSocketService} from "../service/web-socket.service";
import {ChatMessgae} from "../models/ChatMessgae";
import {EmployeeService} from "../service/employee.service";
import {MatTableDataSource} from "@angular/material/table";
import {Computer} from "../models/Computer";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  employee:any;

  constructor(public webSocketService: WebSocketService,
              public employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.webSocketService.openWebSocket();
    this.employeeService.getEmployeeByUsername().subscribe(data => {
      this.employee = data;
    })
  }

  ngOnDestroy(): void {
    this.webSocketService.closeWebSocket();
  }

  sendMessage(sendForm:NgForm){
    const chatMessage = new ChatMessgae(this.employee.username, sendForm.value.Message);
    this.webSocketService.senMessage(chatMessage);
    sendForm.controls.Message.reset();
    console.log(sendForm.value);
  }

}
