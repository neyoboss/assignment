import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {PartService} from "../service/part.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Part} from "../models/Part";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-add-button',
  templateUrl: './add-button.component.html',
  styleUrls: ['./add-button.component.css']
})
export class AddButtonComponent implements OnInit {

  part:Part;

  brand = new FormControl('',[Validators.required]);
  model = new FormControl('',[Validators.required]);
  price = new FormControl('',[Validators.required]);
  quantity = new FormControl('',[Validators.required]);
  description = new FormControl('',[Validators.required]);

  constructor(private partService: PartService,
              private dialog:MatDialogRef<AddButtonComponent>) {
  }

  ngOnInit(): void {
  }

  onAdd():void{
    this.part = new Part();
    this.part.brand = this.brand.value.toString();
    this.part.model = this.model.value.toString();
    this.part.price = this.price.value
    this.part.quantity = this.quantity.value;
    this.part.description = this.description.value.toString();

    this.partService.addPart(this.part).subscribe(()=>{
      console.log("Part added")
    },
      (error : HttpErrorResponse)=>{
      console.log(error)
      });
    location.reload();
  }

  onCancel():void{
    this.dialog.close();
  }
}
