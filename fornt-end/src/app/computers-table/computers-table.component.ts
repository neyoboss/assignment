import {Component, OnInit} from '@angular/core';
import {Computer} from "../models/Computer";
import {ComputerService} from "../service/computer.service";
import {MatTableDataSource} from "@angular/material/table";
import {HttpErrorResponse} from "@angular/common/http";
import {MatDialog} from "@angular/material/dialog";
import {AddComputerComponent} from "../add-computer/add-computer.component";

@Component({
  selector: 'app-computers-table',
  templateUrl: './computers-table.component.html',
  styleUrls: ['./computers-table.component.css']
})
export class ComputersTableComponent implements OnInit {

  displayedColumns: string[] = ['model', 'problem', 'description', 'initialPrice',
    'finalPrice', 'isFixed', 'isAssigned'];

  dataSource: MatTableDataSource<Computer>;
  computerList: any;

  constructor(private computerService: ComputerService,
              private dialog:MatDialog) {
  }

  ngOnInit(): void {
    this.computerService.getComputers().subscribe((data) => {
        console.log(data);
        this.computerList = data;
        this.dataSource = new MatTableDataSource<Computer>(this.computerList);

      },
      (error: HttpErrorResponse) => {
        console.log(error);
      })
  }

  onAddComputer(){
    this.dialog.open(AddComputerComponent);
  }
}
