package com.example.individualproject.config;

import com.example.individualproject.model.Employee;
import com.example.individualproject.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;

@Service
@RequiredArgsConstructor
public class AuthenticationEmployeeService implements UserDetailsService {

    private final EmployeeService employeeService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Employee employee = employeeService.findByUsername(username);

        if (employee == null)
        {
            throw new UsernameNotFoundException(username);
        }

        return new org.springframework.security.core.userdetails.User(employee.getUsername(), employee.getPassword(), getAuthorities(employee.getEmployeeRole().toString()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(String role){
        return Arrays.asList(new SimpleGrantedAuthority(role));
    }
}
