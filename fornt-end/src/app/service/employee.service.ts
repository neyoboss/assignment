import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Employee} from "../models/Employee";
import {environment} from "../../environments/environment";
import {LoginService} from "./login.service";

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  private apiEmployee = environment.apiEmployee;

  constructor(private http:HttpClient,
              private auth:LoginService) { }

  public getEmployees(): Observable<any>{
    return this.http.get<any>(`${this.apiEmployee}`);
  }

  public getEmployeeById(id:number): Observable<any>{
    return this.http.get<any>(`${this.apiEmployee}/${id}`);
  }

  public getEmployeeByUsername():Observable<any>{
    const params = new HttpParams({
      fromObject: {
        username:this.auth.currentAccount.Username
      }
    })
    return this.http.get<any>(`${this.apiEmployee}/byUsername`,{params: params});
  }

  public addEmployee(employee:Employee):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`${this.apiEmployee}/addEmployee`,employee,{headers:httpOptions.headers});
  }

  public updateEmployee(employee:Employee):Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post(`${this.apiEmployee}/addEmployee`,employee,{headers:httpOptions.headers});
  }
}
