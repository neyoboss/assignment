package com.example.individualproject.repository;

import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Part;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;

class PartRepositoryTest {

    @Mock
    private IPartRepository repository;
    private AutoCloseable autoCloseable;
    private PartRepository partRepository;

    @BeforeEach
    void setUp(){
        autoCloseable = MockitoAnnotations.openMocks(this);
        partRepository = new PartRepository(repository);
    }

    @AfterEach
    void tearDown() throws Exception{
        autoCloseable.close();
    }

    @Test
    void getAllParts() {
        partRepository.getAllParts();
        verify(repository).findAll();
    }

    @Test
    void addPart() {
        Part part = new Part("AMD","Ryzen 5", 400, "it is CPU", 15);
        repository.saveAndFlush(part);
        ArgumentCaptor<Part> argumentCaptor = ArgumentCaptor.forClass(Part.class);
        verify(repository).saveAndFlush(argumentCaptor.capture());
        Part capturedPart = argumentCaptor.getValue();
        assertThat(capturedPart).isEqualTo(part);
    }

    @Test
    void getPartById() {
        Part part = new Part("AMD","Ryzen 5", 400, "it is CPU", 15);
        partRepository.saveAndFlush(part);
        repository.saveAndFlush(part);
        Part checkPart = partRepository.getPartById(1);
        verify(repository).getPartById(1);
    }

}