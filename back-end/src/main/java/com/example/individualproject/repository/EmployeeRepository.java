package com.example.individualproject.repository;

import com.example.individualproject.dalInterface.IEmployeeDal;
import com.example.individualproject.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class EmployeeRepository implements IEmployeeDal {
    @Autowired
    IEmployeeRepository repo;

    public EmployeeRepository(IEmployeeRepository repository) {
        this.repo = repository;
    }

    @Override
    public List<Employee> getAllEmployees() {
        return repo.findAll();
    }

    @Override
    public Employee saveAndFlush(Employee employee) {
        return repo.saveAndFlush(employee);
    }

    @Override
    public Employee getEmployeeById(long id) {
        return repo.getEmployeeById(id);
    }

    @Override
    public Employee findByUsername(String username) {
        return repo.findByUsername(username);
    }

    @Override
    public Employee getEmployeeByUsername(String username) {
        return repo.getEmployeeByUsername(username);
    }

}
