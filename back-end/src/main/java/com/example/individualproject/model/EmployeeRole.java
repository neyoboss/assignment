package com.example.individualproject.model;

public enum EmployeeRole {
    ADMIN,
    EMPLOYEE
}
