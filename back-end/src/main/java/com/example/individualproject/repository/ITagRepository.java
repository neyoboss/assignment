package com.example.individualproject.repository;

import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ITagRepository extends JpaRepository<Tag,Long> {
    List<Tag> getByPart(Part part);
}
