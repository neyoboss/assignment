package com.example.individualproject.controller;

import com.example.individualproject.DTO.ComputerDTO;
import com.example.individualproject.DTO.PartDTO;
import com.example.individualproject.DTO.TagDTO;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;
import com.example.individualproject.model.Part;
import com.example.individualproject.service.IComputerService;
import com.example.individualproject.service.IEmployeeService;
import com.example.individualproject.service.IPartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("computer")
public class ComputerController {

    @Autowired
    private IComputerService service;

    @Autowired
    private IPartService partService;

    @Autowired
    private IEmployeeService employeeService;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<ComputerDTO> getAllComputers(){
        return  service.getAllComputers();
    }

    @RequestMapping(value = "/{computerId}",method = RequestMethod.GET)
    public @ResponseBody ComputerDTO getComputerDTOById(@PathVariable() long computerId){
        return  service.getComputerDTOById(computerId);
    }

    @RequestMapping(value = "/addComputer", method = RequestMethod.POST)
    public @ResponseBody String computerAdded(@RequestBody Computer computer){
        service.saveAndFlush(computer);
        return "computer added";
    }

    @RequestMapping(value = "/{employeeId}", method = RequestMethod.GET)
    public @ResponseBody List<ComputerDTO>getByEmployee(@PathVariable()long employeeId){
        Employee employee = employeeService.getEmployeeById(employeeId);
        return service.getByEmployee(employee);
    }

    @RequestMapping(value = "assignComputer/{employeeId}", method = RequestMethod.POST)
    public @ResponseBody String computerAssigned(@PathVariable()long employeeId, @RequestBody List<Computer> computers){
        Employee employee = employeeService.getEmployeeById(employeeId);
        for (Computer c: computers){
            c.setEmployee(employee);
        }
        employee.setComputerList(computers);
        employeeService.saveAndFlush(employee);
        return "Computer assigned";
    }

    @RequestMapping(value = "assignPart", method = RequestMethod.POST)
    public @ResponseBody String partAdded(@RequestParam long computerId, @RequestParam long partId, @RequestParam boolean fixed){
        Computer computer = service.getComputerById(computerId);
        Part part = partService.getPartById(partId);

        computer.getUsedPartsToFix().add(part);
        part.getComputer().add(computer);

        double quantity = part.getQuantity()-1;
        part.setQuantity(quantity);

        long finalPrice = computer.getUsedPartsToFix().stream().mapToLong(parts -> (long) parts.getPrice()).sum();
        computer.setFinalPrice(finalPrice);

        computer.setFixed(fixed);

        partService.saveAndFlush(part);
        service.saveAndFlush(computer);
        return "part assigned";
    }

    @GetMapping(value = "/getAssignedParts/{computerId}")
    public @ResponseBody List<PartDTO> getPartsByComputer(@PathVariable long computerId) {
        Computer computer = service.getComputerById(computerId);
        return partService.getByComputer(computer);
    }

    @PostMapping(value = "/removePartFromPc")
    public @ResponseBody String partRemoved(@RequestParam long computerId, @RequestParam long partId){
        Computer computer = service.getComputerById(computerId);
        Part part = partService.getPartById(partId);

        computer.getUsedPartsToFix().remove(part);
        part.getComputer().remove(computer);

        double quantity = part.getQuantity()+1;
        part.setQuantity(quantity);

        long finalPrice = computer.getUsedPartsToFix().stream().mapToLong(parts -> (long) parts.getPrice()).sum();
        computer.setFinalPrice(finalPrice);

        partService.saveAndFlush(part);
        service.saveAndFlush(computer);
        return "Part removed";
    }

}
