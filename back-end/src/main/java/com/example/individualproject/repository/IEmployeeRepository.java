package com.example.individualproject.repository;

import com.example.individualproject.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmployeeRepository extends JpaRepository<Employee,Long> {
    Employee getEmployeeById(long id);
    Employee getEmployeeByUsernameAndPassword(String username, String password);
    Employee findByUsername(String username);
    Employee getEmployeeByUsername(String username);
}
