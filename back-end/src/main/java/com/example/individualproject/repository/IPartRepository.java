package com.example.individualproject.repository;

import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Part;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IPartRepository extends JpaRepository<Part,Long> {
    Part getPartById(long id);
    List<Part>getByComputer(Computer computer);
}
