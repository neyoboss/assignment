import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {PartService} from "../service/part.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-delete-button',
  templateUrl: './delete-button.component.html',
  styleUrls: ['./delete-button.component.css']
})
export class DeleteButtonComponent{

  constructor(public dialog: MatDialogRef<DeleteButtonComponent>,
              @Inject(MAT_DIALOG_DATA)public data: any, public partService:PartService) { }

  private subs = new Subscription();

   onCloseClick(): void {
    this.dialog.close();
   }

   onDelete(id:number):void{
     this.subs.add(this.partService.deletePart(id).subscribe(() => console.log("Deleted")))
     console.log(this.data.id);
   }
}
