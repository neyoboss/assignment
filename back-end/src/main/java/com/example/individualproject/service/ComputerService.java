package com.example.individualproject.service;

import com.example.individualproject.DTO.ComputerDTO;
import com.example.individualproject.dalInterface.IComputerDal;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ComputerService implements IComputerService{
    @Autowired
    IComputerDal dal;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public List<ComputerDTO> getAllComputers() {
        return dal.getAllComputers()
                .stream()
                .map(this::EntityToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public ComputerDTO saveAndFlush(Computer computer) {
        return EntityToDTO(dal.saveAndFlush(computer));
    }

    @Override
    public Computer getComputerById(long id) {
        return dal.getComputerById(id);
    }

    @Override
    public ComputerDTO getComputerDTOById(long id) {
        return EntityToDTO(dal.getComputerById(id));
    }

    @Override
    public List<ComputerDTO> getByEmployee(Employee employee) {
        return dal.getComputerByEmployee(employee)
                .stream()
                .map(this::EntityToDTO)
                .collect(Collectors.toList());
    }

    private ComputerDTO EntityToDTO(Computer computer) {
        ComputerDTO computerDTO = modelMapper.map(computer, ComputerDTO.class);
        return computerDTO;
    }
}
