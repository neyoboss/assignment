package com.example.individualproject.controller;

import com.example.individualproject.DTO.EmployeeDTO;
import com.example.individualproject.model.Computer;
import com.example.individualproject.model.Employee;
import com.example.individualproject.service.IEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/employee")
public class EmployeeController {
    @Autowired
    IEmployeeService service;

    @RequestMapping(method = RequestMethod.GET)
    public @ResponseBody List<EmployeeDTO> getAllEmployees(){
        return service.getAllEmployees();
    }

    @RequestMapping(value = "/{employeeId}",method = RequestMethod.GET)
    public @ResponseBody EmployeeDTO employeeById(@PathVariable()long employeeId){
        return service.getEmployeeDTOById(employeeId);
    }

    @RequestMapping(value = "/byUsername", method = RequestMethod.GET)
    public @ResponseBody EmployeeDTO employeeByUsername(@RequestParam String username){
        return service.getEmployeeByUsername(username);
    }

    @RequestMapping(value = "/addEmployee",method = RequestMethod.POST)
    public @ResponseBody String saveAndFlush(@RequestBody Employee employee){
        service.saveAndFlush(employee);
        return "employee added";
    }

    @RequestMapping(value = "/giveComputer/{employeeId}",method = RequestMethod.POST)
    public @ResponseBody String giveComputerToEmployee(@PathVariable()long employeeId, @RequestBody List<Computer>computers){
        Employee employee = service.getEmployeeById(employeeId);
        for (Computer c : computers){
            c.setEmployee(employee);
        }
        employee.setComputerList(computers);
        service.saveAndFlush(employee);
        return "computer given";
    }
}
