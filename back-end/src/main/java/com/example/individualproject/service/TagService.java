package com.example.individualproject.service;

import com.example.individualproject.DTO.TagDTO;
import com.example.individualproject.dalInterface.ITagDal;
import com.example.individualproject.model.Part;
import com.example.individualproject.model.Tag;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TagService implements ITagService{
    ITagDal dal;

    @Autowired
    ModelMapper modelMapper;

    @Autowired
    public TagService(ITagDal dal){
        this.dal = dal;
    }

    @Override
    public List<Tag> getAllTags() {
      return dal.getAllTags();
    }

    @Override
    public void addTag(Tag tag) {
        dal.addTag(tag);
    }

    @Override
    public Tag findTagById(long id) {
        return dal.getTagById(id);
    }

    @Override
    public List<TagDTO> saveAndFlush(List<Tag> tagList) {
        return dal.saveAndFlush(tagList).stream().map(this::EntityToDTO).collect(Collectors.toList());
    }

    @Override
    public List<TagDTO> getByPart(Part part) {
        return dal.getByPart(part).stream().map(this::EntityToDTO).collect(Collectors.toList());
    }

    @Override
    public void deleteTag(long id) {
        dal.deleteTag(id);
    }

    private TagDTO EntityToDTO(Tag tag) {
        TagDTO tagDTO = modelMapper.map(tag, TagDTO.class);
        return tagDTO;
    }
}
