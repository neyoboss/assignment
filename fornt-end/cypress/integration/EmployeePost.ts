describe('Posting employee', ()=>{
  context('When I send POST /addEmployee', ()=>{
    it('Should create employee', ()=>{
      cy.request({
        method: 'POST',
        url: 'http://localhost:8080/employee/addEmployee',
        body:{
          firstName:"Jason",
          lastName:"Statham",
          username:"jStatham",
          password:123,
          employeeRole:"ADMIN"
        }
      })
        .should((response) =>{
          expect((response.status)).eq(200);
        })
    })
  })
})
