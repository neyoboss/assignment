package com.example.individualproject.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@AllArgsConstructor
public  class Part {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "partId")
    private long id;

    @OneToMany(mappedBy = "part",
                fetch = FetchType.LAZY,
                cascade = CascadeType.ALL)
     private List<Tag>tags;

    @Column(name = "brand")
     private String brand;

    @Column(name = "model")
     private String model;

    @Column(name = "price")
     private double price;

    @Column(name = "quantity")
     private double quantity;

    @Column(name = "description")
     private String description;

    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY,cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
    })
    @JoinTable(name = "computer_part",
    joinColumns = {@JoinColumn(name = "partId")},
    inverseJoinColumns = {@JoinColumn(name = "computerId")})
    private List<Computer> computer;

    public long getId() {
        return id;
    }

    public List<Computer> getComputer() {
        return computer;
    }

    public void setComputer(List<Computer> computer) {
        this.computer = computer;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Part(){};

    public Part(String brand, String model, double price, String description, double quantity)
    {
        this.brand = brand;
        this.model = model;
        this.price = price;
        this.description = description;
        this.quantity = quantity;
    }

}
